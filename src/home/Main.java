package home;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import required.DBConnect;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class Main extends Application {
    public double x, y;
    private ChoiceBox anneeBilan;
    private BarChart<String, Number> bilanChart;
    private String an;

    public static void main(String[] args) {
        launch();
    }

    @Override
    public void start(Stage primaryStage) throws IOException, SQLException {
        Parent root = FXMLLoader.load(getClass().getResource("../Home.fxml"));

        //Recuperation et affichage du compte de prof dans main
        Label label = (Label) root.lookup("#profCount");
        DBConnect dbConnect = new DBConnect();

        String query = "SELECT * FROM Professeurs";
        ResultSet res = null;

        res = dbConnect.fetch_in_bdd(query);

        res.last();
        label.setText(String.valueOf(res.getRow()));

        //Recuperation et affichage du compte de mat dans main
        Label label1 = (Label) root.lookup("#matCount");

        String query1 = "SELECT * FROM Matieres";
        ResultSet res1 = null;

        res1 = dbConnect.fetch_in_bdd(query1);

        res1.last();
        label1.setText(String.valueOf(res1.getRow()));

        //Recuperation et affichage du compte de volH dans main
        Label label2 = (Label) root.lookup("#volHCount");

        String query2 = "SELECT * FROM volumeHoraire";
        ResultSet res2 = null;

        res2 = dbConnect.fetch_in_bdd(query2);

        res2.last();
        label2.setText(String.valueOf(res2.getRow()));

        //Contenue Checkbox annee
        anneeBilan = (ChoiceBox) root.lookup("#anneeBilan");

        String reqAn = "SELECT DISTINCT annee FROM volumeHoraire  ORDER BY annee ASC;";
        ResultSet resultSet = dbConnect.fetch_in_bdd(reqAn);
        List<String> listAnnB = new ArrayList<>();

        while (resultSet.next()) {
            listAnnB.add(String.valueOf(resultSet.getInt("annee")));
        }
        if (listAnnB != null) {
            anneeBilan.setItems(FXCollections.observableArrayList(listAnnB));
        }

        //Affichage du chart bar
        bilanChart = (BarChart) root.lookup("#bilanChart");
        if (listAnnB != null) {
            //anneeBilan.setValue(FXCollections.observableArrayList(listAnnB).get(0));
            afficheBilan();
        }

        primaryStage.setTitle("J3P");
        primaryStage.setScene(new Scene(root));
        primaryStage.getIcons().add(new Image("images/icon.png"));

        primaryStage.initStyle(StageStyle.UNDECORATED);

        root.setOnMousePressed(event -> {
            x = event.getSceneX();
            y = event.getSceneY();
        });
        root.setOnMouseDragged(event -> {
            primaryStage.setX(event.getX() - x);
            primaryStage.setY(event.getY() - y);
        });

        primaryStage.show();

    }

    //fonction affichage chartBar
    public void afficheBilan() {
        // Créez des instances de XYChart.Series pour représenter les séries de données
        XYChart.Series<String, Number> series1 = new XYChart.Series<>();
        series1.setName("Professeurs");


        //Recupérer les donnes dans la bdd
        DBConnect dbConnect = new DBConnect();
        ResultSet rs;
        an = null;
        List<String> listAnnB = new ArrayList<>();
        try {
            rs = dbConnect.fetch_in_bdd("SELECT DISTINCT annee FROM volumeHoraire;");
            if (rs.next()) {
                an = String.valueOf(rs.getInt("annee"));
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        String sql = "SELECT\n" +
                "  p.matricule,\n" +
                "  p.nom,\n" +
                "  SUM(m.nbrHeure * p.tauxHoraire) AS montant\n" +
                "FROM\n  " +
                "Professeurs p\n" +
                "  JOIN volumeHoraire vh ON p.matricule = vh.matricule\n" +
                "  JOIN Matieres m ON vh.numMat = m.numMat\n" +
                " WHERE annee = " + an +
                " GROUP BY\n" +
                "  p.matricule,\n" +
                "  p.nom;\n";

        if (an != null) {
            try {
                ResultSet resultSet = dbConnect.fetch_in_bdd(sql);
                while (resultSet.next()) {
                    // Ajoutez des données à chaque série en utilisant la méthode getData().add()
                    series1.getData().add(new XYChart.Data<>(resultSet.getString("nom"), resultSet.getBigDecimal("montant")));
                }
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }


        // Ajoutez les séries de données au BarChart en utilisant la méthode getData().addAll()
        bilanChart.getData().addAll(series1);
    }
}
