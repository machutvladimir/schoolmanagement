package Model;

public class VH {
    private final String matProf;
    private final String numMatiere;
    private final int anneeVH;

    public VH(int anneeVH, String matProf, String numMatiere){
        this.anneeVH = anneeVH;
        this.matProf = matProf;
        this.numMatiere = numMatiere;
    }

    public int getAnneeVH() {
        return anneeVH;
    }
    public String getNumMatiere(){
        return numMatiere;
    }
    public String getMatProf(){
        return matProf;
    }
}
