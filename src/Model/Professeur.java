package Model;

public class Professeur {
    private final String matriculeProf;
    private final String nomProf;
    private final int tauxHoraire;

    public Professeur(String matriculeProf, String nomProf, int tauxHoraire) {
        this.matriculeProf = matriculeProf;
        this.nomProf = nomProf;
        this.tauxHoraire = tauxHoraire;
    }

    public String getMatriculeProf() {
        return matriculeProf;
    }

    public String getNomProf() {
        return nomProf;
    }

    public int getTauxHoraire() {
        return tauxHoraire;
    }

}
