package Model;

public class Matiere {
    private final String numMat;
    private final String designMat;
    private final int nbrHeureMat;

    public Matiere(String numMat, String designMat, int nbrHeureMat){
        this.numMat = numMat;
        this.designMat = designMat;
        this.nbrHeureMat = nbrHeureMat;
    }

    public String getNumMat() {
        return numMat;
    }

    public String getDesignMat() {
        return designMat;
    }

    public int getNbrHeureMat() {
        return nbrHeureMat;
    }
}
