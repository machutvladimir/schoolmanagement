package required;

import java.sql.*;

public class DBConnect {
    public static String HOST = "127.0.0.1";
    public static int PORT = 3306;
    public static String DB_NAME = "J3P";
    public static String USERNAME = "root";
    public static String PASSWORD = "";
    public static Connection connection;

    public static Connection getConnect() {

        try {
            connection = DriverManager.getConnection(String.format("jdbc:mysql://%s:%d/%s", HOST, PORT, DB_NAME), USERNAME, PASSWORD);
        } catch (SQLException e) {
            System.out.println("Connexion echoue!!!");
            throw new RuntimeException(e);
        }

        return connection;
    }

    public ResultSet fetch_in_bdd(String request) throws SQLException {
        Statement statement = getConnect().createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
        ResultSet resultat = statement.executeQuery(request);

        return resultat;
    }

    public boolean fkCheck(String request) throws SQLException {
        Statement statement = getConnect().createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
        boolean resultat = statement.execute(request);

        return resultat;
    }

    public int edit_bdd(String request, String add1, String add2, int add3) throws SQLException {
        PreparedStatement preparedStatement = getConnect().prepareStatement(request);
        preparedStatement.setString(1, add1);
        preparedStatement.setString(2, add2);
        preparedStatement.setInt(3, add3);

        return preparedStatement.executeUpdate();
    }

    public int edit_bdd(String request, String add2, int add3, String primKey) throws SQLException {
        PreparedStatement preparedStatement = getConnect().prepareStatement(request);
        preparedStatement.setString(1, add2);
        preparedStatement.setInt(2, add3);
        preparedStatement.setString(3, primKey);

        return preparedStatement.executeUpdate();
    }

    public int edit_bdd(String request, String add, String add2, int add3, String primKey, String primKey2) throws SQLException {
        PreparedStatement preparedStatement = getConnect().prepareStatement(request);
        preparedStatement.setString(1, add);
        preparedStatement.setString(2, add2);
        preparedStatement.setInt(3, add3);
        preparedStatement.setString(4, primKey);
        preparedStatement.setString(5, primKey2);

        return preparedStatement.executeUpdate();
    }

    public int edit_bdd(String s, String add, String add2, String s1, String s2) throws SQLException {
        PreparedStatement preparedStatement = getConnect().prepareStatement(s);
        preparedStatement.setString(1, add);
        preparedStatement.setString(2, add2);
        preparedStatement.setString(3, s1);
        preparedStatement.setString(4, s2);

        return preparedStatement.executeUpdate();
    }

    public int edit_bdd(String s, String add, int add2, String s1, String s2) throws SQLException {
        PreparedStatement preparedStatement = getConnect().prepareStatement(s);
        preparedStatement.setString(1, add);
        preparedStatement.setInt(2, add2);
        preparedStatement.setString(3, s1);
        preparedStatement.setString(4, s2);

        return preparedStatement.executeUpdate();
    }

    public int edit_bdd(String s, String add, String s1, String s2) throws SQLException {
        PreparedStatement preparedStatement = getConnect().prepareStatement(s);
        preparedStatement.setString(1, add);
        preparedStatement.setString(2, s1);
        preparedStatement.setString(3, s2);

        return preparedStatement.executeUpdate();
    }

    public int edit_bdd(String s, int add, String s1, String s2) throws SQLException {
        PreparedStatement preparedStatement = getConnect().prepareStatement(s);
        preparedStatement.setInt(1, add);
        preparedStatement.setString(2, s1);
        preparedStatement.setString(3, s2);

        return preparedStatement.executeUpdate();
    }

    public int removeItemFromBdd(String request, String primKey) throws SQLException {
        PreparedStatement preparedStatement = getConnect().prepareStatement(request);
        preparedStatement.setString(1, primKey);

        return preparedStatement.executeUpdate();
    }

    public int removeItemFromBdd(String request, String primKey1, String primKey2) throws SQLException {
        PreparedStatement preparedStatement = getConnect().prepareStatement(request);
        preparedStatement.setString(1, primKey1);
        preparedStatement.setString(2, primKey2);

        return preparedStatement.executeUpdate();
    }
}
