package Controller;

import Model.Professeur;
import Model.VH;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import javafx.animation.FadeTransition;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.BooleanBinding;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Duration;
import required.DBConnect;

import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import static Controller.DashboardController.createCell;


public class ProfController implements Initializable {
    private final List<String> allPrimKey = new ArrayList<>();
    public ChoiceBox matriculeEx;
    public ChoiceBox nomP;
    public ChoiceBox anneeVH;
    @FXML
    Parent parent;
    @FXML
    Label Matricule;
    @FXML
    Label Nom;
    @FXML
    Label TauxHoraire;
    @FXML
    Button send;
    private TableView<Professeur> tableListProf;
    private ObservableList<Professeur> professeurList;
    private TableColumn<Professeur, String> matriculeProf;
    private TableColumn<Professeur, String> nomProf;
    private TableColumn<Professeur, Integer> tauxHoraireProf;
    private TableView<VH> tableListeVH;

    private ObservableList<VH> VHList;
    private TableColumn<VH, String> matProfCol;
    private TableColumn<VH, String> numMatiereCol;
    private TableColumn<VH, Integer> anneeCol;
    @FXML
    private TextField matriculeAdd;
    @FXML
    private TextField nom;
    @FXML
    private TextField tauxHoraire;

    public void requestParent(Parent parent) {
        this.parent = parent;
    }

    public void request(TableView<Professeur> tableListProf,
                        ObservableList<Professeur> professeurList,
                        TableColumn<Professeur, String> matriculeProf,
                        TableColumn<Professeur, String> nomProf,
                        TableColumn<Professeur, Integer> tauxHoraireProf,
                        TableView<VH> tableListeVH,
                        ObservableList<VH> VHList,
                        TableColumn<VH, String> matProfCol,
                        TableColumn<VH, String> numMatiereCol,
                        TableColumn<VH, Integer> anneeCol) {
        this.tableListProf = tableListProf;
        this.professeurList = professeurList;
        this.matriculeProf = matriculeProf;
        this.nomProf = nomProf;
        this.tauxHoraireProf = tauxHoraireProf;
        this.tableListeVH = tableListeVH;
        this.VHList = VHList;
        this.matProfCol = matProfCol;
        this.numMatiereCol = numMatiereCol;
        this.anneeCol = anneeCol;

    }

    /*--------------Les bouttons dans popup---------------*/
    //Envoie de donnee dans ajout
    public void send(ActionEvent actionEvent) throws SQLException {
        DBConnect dbConnect = new DBConnect();
        String query = "INSERT INTO Professeurs (matricule, nom, tauxHoraire) VALUES (?,?,?)";
        String matVal = "";
        String nomVal = "";
        int tauxHVal = 0;

        TextField textField1 = (TextField) parent.lookup("#matriculeAdd");
        matVal = textField1.getText();

        TextField textField2 = (TextField) parent.lookup("#nom");
        nomVal = textField2.getText();

        TextField textField3 = (TextField) parent.lookup("#tauxHoraire");
        tauxHVal = Integer.parseInt(textField3.getText());

        int sucAjout = dbConnect.edit_bdd(query, matVal, nomVal, tauxHVal);

        if (sucAjout == 1) {
            closePopup(actionEvent);
            alertProfSuccess();
            ajoutProfAfficheList();
        } else {
            alertProfError();
        }

    }

    public void sendUpdate(ActionEvent actionEvent) throws SQLException {
        String matVal = "";
        String nomVal = "";
        int tauxHVal = 0;
        String query = "UPDATE `Professeurs` SET `nom`=?,`tauxHoraire`=? WHERE matricule = ?";

        TextField textField1 = (TextField) parent.lookup("#matricule");
        matVal = textField1.getText();

        TextField textField2 = (TextField) parent.lookup("#nom");
        nomVal = textField2.getText();

        TextField textField3 = (TextField) parent.lookup("#tauxHoraire");
        tauxHVal = Integer.parseInt(textField3.getText());


        DBConnect dbConnect = new DBConnect();

        if (dbConnect.edit_bdd(query, nomVal, tauxHVal, matVal) == 0) alertProfError();
        else {
            closePopup(actionEvent);
            ajoutProfAfficheList();
            ajoutVHAfficheList();
            alertProfSuccess();
        }
    }

    public void removeItem(ActionEvent actionEvent) throws SQLException {
        DBConnect dbConnect = new DBConnect();
        String query = "DELETE FROM Professeurs WHERE matricule=?";
        String queryForeign = "DELETE FROM volumeHoraire WHERE matricule=?";
        String matVal = "";


        TextField textField1 = (TextField) parent.lookup("#matricule");
        matVal = textField1.getText();

        dbConnect.fkCheck("SET FOREIGN_KEY_CHECKS=0;");
        dbConnect.removeItemFromBdd(queryForeign, matVal);
        dbConnect.fkCheck("SET FOREIGN_KEY_CHECKS=1;");
        if (dbConnect.removeItemFromBdd(query, matVal) == 0) {
            alertProfError();
        } else {
            closePopup(actionEvent);
            ajoutProfAfficheList();
            ajoutVHAfficheList();
            alertProfSuccess();
        }
    }

    /*-----------------------Validate input----------------------------------------*/

    @FXML
    private void closePopup(ActionEvent event) {
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        FadeTransition ft = new FadeTransition(Duration.millis(1000), stage.getScene().getRoot());
        ft.setFromValue(1.0);
        ft.setToValue(0.0);
        ft.setOnFinished(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                stage.close();
            }
        });
        ft.play();
    }

    void alertProfSuccess() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Succès");
        alert.setHeaderText(null);
        alert.initModality(Modality.APPLICATION_MODAL);
        alert.setContentText("L'opération a été effectuée avec succès !");
        alert.showAndWait();
    }

    void alertProfError() {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Erreur");
        alert.setHeaderText(null);
        alert.setContentText("Une erreur est survenu!\nVeuiller réessayer à nouveau s'il vous plaît!");
        alert.showAndWait();
    }

    public boolean validateMatricule(String st) {
        boolean b = false;
        for (String primkey : allPrimKey) b = b | (primkey.equalsIgnoreCase(st));
        // Le matricule est vide ou nul
        return !st.trim().isEmpty() && st.trim().matches("[a-zA-Z0-9_]+") && !b;
    }

    public boolean validateNom(String st) {
        // Le nom est vide ou nul
        return !st.trim().isEmpty() && st.trim().matches("[a-z A-Z]+");
    }

    public boolean validateVolH(String st) {
        // Le nom est vide ou nul
        return !st.trim().isEmpty() && st.trim().matches("[0-9]+");
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        //Recuperation de toute les cles primaire "matricule" dans le bdd vers une variable liste array "allPrimKey"
        DBConnect dbConnect = new DBConnect();
        String sql = "SELECT matricule FROM Professeurs";

        try {
            ResultSet resultSet = dbConnect.fetch_in_bdd(sql);
            while (resultSet.next()) {
                allPrimKey.add(resultSet.getString("matricule"));
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        if (matriculeAdd != null)
            matriculeAdd.textProperty().addListener((observable, oldValue, newValue) -> {
                boolean b = false;
                for (String primkey : allPrimKey) {
                    b |= (primkey.equals(newValue));
                }
                //System.out.println("12".equals("12") +"\n");
                if (newValue.trim().isEmpty() || !newValue.trim().matches("[a-zA-Z0-9_]+") || b) {
                    matriculeAdd.setStyle("-fx-border-color: red ; -fx-border-width: 2px ;");
                } else {
                    matriculeAdd.setStyle("");
                }
            });
        if (nom != null) {
            nom.textProperty().addListener((observable, oldValue, newValue) -> {
                if (newValue.trim().isEmpty() || !newValue.trim().matches("[a-z A-Z]+")) {
                    nom.setStyle("-fx-border-color: red ; -fx-border-width: 2px ;");
                } else {
                    nom.setStyle("");
                }
            });
        }

        if (tauxHoraire != null) {
            tauxHoraire.textProperty().addListener((observable, oldValue, newValue) -> {
                if (newValue.trim().isEmpty() || !newValue.trim().matches("[0-9]+")) {
                    tauxHoraire.setStyle("-fx-border-color: red ; -fx-border-width: 2px ;");
                } else {
                    tauxHoraire.setStyle("");
                }
            });
        }

        // Créer une expression binaire qui vérifie si les entrées sont valides ou non
        if (matriculeAdd != null) {
            BooleanBinding inputsValid = Bindings.createBooleanBinding(() -> {

                boolean inputM = validateMatricule(matriculeAdd.getText());
                boolean inputN = validateNom(nom.getText());
                boolean inputVH = validateVolH(tauxHoraire.getText());

                return inputM && inputN && inputVH;
            }, matriculeAdd.textProperty(), nom.textProperty(), tauxHoraire.textProperty());
            // Lier la propriété "disable" du bouton à l'expression d'entrées valides
            send.disableProperty().bind(inputsValid.not());
        } else {
            if (nom != null && tauxHoraire != null) {
                BooleanBinding inputsValid = Bindings.createBooleanBinding(() -> {
                    boolean inputN = validateNom(nom.getText());
                    boolean inputVH = validateVolH(tauxHoraire.getText());

                    return inputN && inputVH;
                }, nom.textProperty(), tauxHoraire.textProperty());
                // Lier la propriété "disable" du bouton à l'expression d'entrées valides
                send.disableProperty().bind(inputsValid.not());
            }
        }
    }

    /*--------------------------RELOAD PROF VIEW TABLE-----------------------------*/

    public ObservableList<Professeur> ajoutProfesseurList() throws SQLException {
        ObservableList<Professeur> listData = FXCollections.observableArrayList();

        String sql = "SELECT * FROM Professeurs ORDER BY dateAjout";

        DBConnect dbConnect = new DBConnect();
        ResultSet resultSet = dbConnect.fetch_in_bdd(sql);

        Professeur professeurData;
        while (resultSet.next()) {
            professeurData = new Professeur(resultSet.getString("matricule"),
                    resultSet.getString("nom"),
                    resultSet.getInt("tauxHoraire"));

            listData.add(professeurData);
        }

        return listData;
    }


    public void ajoutProfAfficheList() throws SQLException {
        professeurList = ajoutProfesseurList();

        matriculeProf.setCellValueFactory(new PropertyValueFactory<>("matriculeProf"));
        nomProf.setCellValueFactory(new PropertyValueFactory<>("nomProf"));
        tauxHoraireProf.setCellValueFactory(new PropertyValueFactory<>("tauxHoraire"));

        tableListProf.setItems(professeurList);
    }

    //Reload volume horaire

    public void ajoutVHAfficheList() throws SQLException {

        VHList = ajoutVHList();

        anneeCol.setCellValueFactory(new PropertyValueFactory<>("anneeVH"));
        matProfCol.setCellValueFactory(new PropertyValueFactory<>("matProf"));
        numMatiereCol.setCellValueFactory(new PropertyValueFactory<>("numMatiere"));

        tableListeVH.setItems(VHList);

    }

    public ObservableList<VH> ajoutVHList() throws SQLException {
        ObservableList<VH> listData = FXCollections.observableArrayList();

        String sql = "SELECT * FROM volumeHoraire ORDER BY dateAjout";

        DBConnect dbConnect = new DBConnect();
        try (ResultSet resultSet = dbConnect.fetch_in_bdd(sql)) {

            VH VHData;
            while (resultSet.next()) {
                VHData = new VH(
                        resultSet.getInt("annee"),
                        resultSet.getString("matricule"),
                        resultSet.getString("numMat")
                );

                listData.add(VHData);
            }
        }

        return listData;
    }
    //Exporter en pdf

    public void exportPDF(ActionEvent actionEvent) throws IOException, DocumentException, SQLException {
        String anneeA = (String) anneeVH.getValue();
        String matPr = (String) matriculeEx.getValue();
        DBConnect dbConnect = new DBConnect();
        String sql = "SELECT\n" +
                "  p.matricule,\n" +
                "  p.nom,\n" +
                "  (m.nbrHeure * p.tauxHoraire) AS montant,\n" +
                " m.design,\n" +
                " m.nbrHeure\n" +
                "FROM\n" +
                "  Professeurs p\n" +
                "  JOIN volumeHoraire vh ON p.matricule = vh.matricule\n" +
                "  JOIN Matieres m ON vh.numMat = m.numMat\n" +
                "WHERE annee = " + anneeA + " AND " +
                " p.matricule = '" + matPr + "';";
        ResultSet resultSet = dbConnect.fetch_in_bdd(sql);
        ResultSet resultSetProf = dbConnect.fetch_in_bdd("SELECT * FROM Professeurs WHERE matricule = '" + matPr + "';");

        Document document = new Document();

        PdfWriter.getInstance(document, new FileOutputStream("Fiche_de_paye_" + nomP.getValue() + ".pdf"));
        document.open();

        Rectangle pageSize = document.getPageSize();
        float pageWidth = pageSize.getWidth();

        float padding = pageWidth / 40;

        Font titreFont = FontFactory.getFont(FontFactory.HELVETICA, 24, Font.BOLD, new BaseColor(255, 255, 255));
        PdfPCell titreCell = new PdfPCell(new Phrase("FICHE DE PAYE", titreFont));
        titreCell.setBackgroundColor(new BaseColor(42, 115, 255));
        titreCell.setPadding(padding);
        titreCell.setBorder(Rectangle.NO_BORDER);
        titreCell.setHorizontalAlignment(Element.ALIGN_CENTER);

        PdfPTable table = new PdfPTable(1);
        table.setWidthPercentage(100);
        table.addCell(titreCell);
        table.setSpacingAfter(10);

        document.add(table);

        resultSetProf.next();

        PdfPTable nomProf = new PdfPTable(1);
        nomProf.setWidthPercentage(100);
        nomProf.addCell(createCell("Nom             :     " + resultSetProf.getString("nom"), false, false));

        PdfPTable prenomProf = new PdfPTable(1);
        prenomProf.setWidthPercentage(100);
        prenomProf.addCell(createCell("Matricule      :     " + resultSetProf.getString("matricule"), false, false));

        PdfPTable annee = new PdfPTable(1);
        annee.setWidthPercentage(100);
        annee.addCell(createCell("Année          :     " + anneeA, false, false));

        annee.setSpacingAfter(15);

        PdfPTable tableau = new PdfPTable(3);
        tableau.setWidthPercentage(100);
        tableau.addCell(createCell("Désignation", true));
        tableau.addCell(createCell("Nombre d'heures", true));
        tableau.addCell(createCell("Montant", true));

        document.add(titreCell);
        document.add(nomProf);
        document.add(prenomProf);
        document.add(annee);
        document.add(tableau);

        int montTotal = 0;
        while (resultSet.next()) {
            PdfPTable rowI = new PdfPTable(3);
            rowI.setWidthPercentage(100);
            rowI.addCell(createCell(resultSet.getString("m.design"), false));
            rowI.addCell(createCell(resultSet.getString("m.nbrHeure"), false));
            rowI.addCell(createCell(resultSet.getString("montant"), false));

            montTotal += resultSet.getInt("montant");
            document.add(rowI);
        }
        PdfPTable rowTotal = new PdfPTable(2);
        rowTotal.setWidthPercentage((float) 200 / 3);
        rowTotal.setHorizontalAlignment(Element.ALIGN_RIGHT);
        rowTotal.addCell(createCell("Total", false));
        rowTotal.addCell(createCell(montTotal + " Ar", false));

        document.add(rowTotal);

        document.close();

        ProcessBuilder processBuilder = new ProcessBuilder("bash", "-c", "open Fiche_de_paye_" + nomP.getValue() + ".pdf");

        processBuilder.redirectErrorStream(true);

        try {
            processBuilder.start();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Succès");
        alert.setHeaderText(null);
        alert.setContentText("Fiche de paie générée avec succès !");

        Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
        stage.setAlwaysOnTop(true);
        closePopup(actionEvent);
        alert.showAndWait();
    }
}

