package Controller;

import Model.Matiere;
import Model.Professeur;
import Model.VH;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import javafx.animation.FadeTransition;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import required.DBConnect;

import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import static java.lang.Integer.parseInt;

public class DashboardController implements Initializable {

    @FXML
    private Button BulletinPaieProf;

    @FXML
    private Button ModifierProf;

    @FXML
    private Button SupProf;

    @FXML
    private Button ajoutMat;

    @FXML
    private Button ajouterProf;

    @FXML
    private Button ajouterVolH;

    @FXML
    private BarChart<String, Number> bilanChart;

    @FXML
    private Button bilanExportPDF;

    @FXML
    private Button exportPDFMat;

    @FXML
    private Button exporterPDFVolH;

    @FXML
    private AnchorPane homePane;

    @FXML
    private TextField searchProf;

    @FXML
    private AnchorPane mainPane;


    @FXML
    private AnchorPane matPane;

    @FXML
    private TableColumn<Professeur, String> matriculeProf;

    @FXML
    private Button menuItemDashboard;

    @FXML
    private Button menuItemMat;

    @FXML
    private Button menuItemProf;

    @FXML
    private Button menuItemVolH;

    @FXML
    private Button modifierMat;

    @FXML
    private Button modifierVolH;

    @FXML
    private TableColumn<Professeur, String> nomProf;


    @FXML
    private AnchorPane profPane;

    @FXML
    private TextField searchMat;


    @FXML
    private CheckBox searchProfByMat;

    @FXML
    private CheckBox searchProfByNom;
    @FXML
    private CheckBox searchMatByNum;
    @FXML
    private CheckBox searchMatByDesign;

    @FXML
    private CheckBox searchVHByNom;
    @FXML
    private CheckBox searchVHByDesign;

    @FXML
    private TextField searchVolH;

    @FXML
    private Button supMat;

    @FXML
    private Button supVolH;

    @FXML
    private TableView<Professeur> tableListProf;

    @FXML
    private TableColumn<Professeur, Integer> tauxHoraireProf;


    @FXML
    private AnchorPane volHPane;

    @FXML
    private TableColumn<Matiere, String> designMatCol;
    @FXML
    private TableColumn<Matiere, String> numMatCol;

    @FXML
    private TableColumn<Matiere, Integer> nbrHeureMatCol;
    @FXML
    private TableView<Matiere> tableListMat;
    @FXML
    private TableView<VH> tableListeVH;

    @FXML
    private TableColumn<VH, Integer> anneeCol;

    @FXML
    private TableColumn<VH, String> matProfCol;
    @FXML
    private TableColumn<VH, String> numMatiereCol;


    private Stage primaryStage;
    private Professeur profSel;
    private Matiere matSel;
    private VH vhSel;

    @FXML
    private ChoiceBox anneeBilan;
    private ObservableList<Professeur> ProfesseurList;
    private ObservableList<Matiere> MatiereList;
    private ObservableList<VH> VHList;

    private int anneeA = 0;

    public static PdfPCell createCell(String content, boolean isHeader) {
        PdfPCell cell;
        if (isHeader) {
            Font titreFont = FontFactory.getFont(FontFactory.HELVETICA, 14, Font.BOLD, new BaseColor(255, 255, 255));
            cell = new PdfPCell(new Phrase(content, titreFont));
        } else {
            cell = new PdfPCell(new Phrase(content));
        }
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setPadding(5);
        cell.setUseBorderPadding(true);

        if (isHeader) {
            cell.setBackgroundColor(new BaseColor(42, 115, 255));
        }

        cell.setBorderColor(new BaseColor(135, 206, 250));

        return cell;
    }

    public static PdfPCell createCell(String content, boolean isHeader, boolean border) {
        PdfPCell cell = new PdfPCell(new Phrase(content));
        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setPadding(5);
        cell.setUseBorderPadding(border);
        cell.setBorderColor(new BaseColor(255, 255, 255));

        if (isHeader) {
            cell.setBackgroundColor(new BaseColor(42, 115, 255));
            cell.setBorderColor(new BaseColor(135, 206, 250));
        }
        return cell;
    }

    public void ajoutMatiereAfficheList() throws SQLException {

        MatiereList = ajoutMatiereList();

        numMatCol.setCellValueFactory(new PropertyValueFactory<>("numMat"));
        designMatCol.setCellValueFactory(new PropertyValueFactory<>("designMat"));
        nbrHeureMatCol.setCellValueFactory(new PropertyValueFactory<>("nbrHeureMat"));

        tableListMat.setItems(MatiereList);

        searchMatByDesign.setSelected(true);
        searchMatByNum.setSelected(true);

        //Recherche item
        FilteredList<Matiere> filteredList = new FilteredList<>(MatiereList, b -> true);
        searchMat.textProperty().addListener((observable, oldValue, newValue) -> {
            filteredList.setPredicate(Matiere -> {
                if (newValue.isEmpty() || newValue.isBlank() || newValue == null) return true;

                String searchKeyword = newValue.toLowerCase();

                if (searchMatByNum.isSelected() && (Matiere.getNumMat().toLowerCase().indexOf(searchKeyword) > -1))
                    return true;
                return searchMatByDesign.isSelected() && (Matiere.getDesignMat().toLowerCase().indexOf(searchKeyword) > -1);
            });
        });
        SortedList<Matiere> sortedList = new SortedList<>(filteredList);

        sortedList.comparatorProperty().bind(tableListMat.comparatorProperty());

        tableListMat.setItems(sortedList);
    }

    public void ajoutVHAfficheList() throws SQLException {

        VHList = ajoutVHList();

        anneeCol.setCellValueFactory(new PropertyValueFactory<>("anneeVH"));
        matProfCol.setCellValueFactory(new PropertyValueFactory<>("matProf"));
        numMatiereCol.setCellValueFactory(new PropertyValueFactory<>("numMatiere"));

        tableListeVH.setItems(VHList);

        searchVHByNom.setSelected(true);
        searchVHByDesign.setSelected(true);

        //Recherche item
        FilteredList<VH> filteredList = new FilteredList<>(VHList, b -> true);
        searchVolH.textProperty().addListener((observable, oldValue, newValue) -> {
            filteredList.setPredicate(VH -> {
                if (newValue.isEmpty() || newValue.isBlank() || newValue == null) return true;

                String searchKeyword = newValue.toLowerCase();

                if (searchVHByNom.isSelected() && (VH.getMatProf().toLowerCase().contains(searchKeyword)))
                    return true;
                return searchVHByDesign.isSelected() && (VH.getNumMatiere().toLowerCase().contains(searchKeyword));
            });
        });
        SortedList<VH> sortedList = new SortedList<>(filteredList);

        sortedList.comparatorProperty().bind(tableListeVH.comparatorProperty());

        tableListeVH.setItems(sortedList);
    }

    private void chargerDonneesEnTempsReel() {
        String sql = "SELECT COUNT(*) FROM volumeHoraire;";
        DBConnect dbConnect = new DBConnect();
        int rowCount = 0;
        try {
            ResultSet resultSet1 = dbConnect.fetch_in_bdd(sql);
            if (resultSet1.next()) {
                rowCount = resultSet1.getInt(1);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        if (rowCount > 0) {
            anneeBilan.valueProperty().addListener((observable, oldValue, newValue) -> {
                XYChart.Series<String, Number> series1 = bilanChart.getData().get(0);
                XYChart.Series<String, Number> series = new XYChart.Series<>();
                //Recupérer les donnes dans la bdd
                String sqlB = "SELECT\n" +
                        "  p.matricule,\n" +
                        "  p.nom,\n" +
                        "  SUM(m.nbrHeure * p.tauxHoraire) AS montant\n" +
                        "FROM\n" +
                        "  Professeurs p\n" +
                        "  JOIN volumeHoraire vh ON p.matricule = vh.matricule\n" +
                        "  JOIN Matieres m ON vh.numMat = m.numMat\n" +
                        "WHERE annee = " + newValue +
                        " GROUP BY\n" +
                        "  p.matricule,\n" +
                        "  p.nom;\n";
                try {
                    ResultSet resultSet = dbConnect.fetch_in_bdd(sqlB);
                    bilanChart.getData().remove(series1);
                    while (resultSet.next()) {
                        series.getData().add(new XYChart.Data<>(resultSet.getString("nom"), resultSet.getBigDecimal("montant")));
                    }
                    series.setName("Professeur");
                    bilanChart.getData().add(series);
                } catch (SQLException e) {
                    throw new RuntimeException(e);
                }

                if (newValue != null) {
                    anneeA = Integer.parseInt(newValue.toString());
                } else {
                    System.err.println("La valeur de newValue n'est pas un entier valide.");
                    anneeA = 0; // Valeur par défaut
                }
            });
            bilanExportPDF.setDisable(false);
        } else {
            if (bilanChart.getData().size() > 0) {
                bilanChart.getData().remove(bilanChart.getData().get(0));
                XYChart.Series<String, Number> series1 = new XYChart.Series<>();
                series1.setName("Professeurs");
                bilanChart.getData().addAll(series1);
            }

            bilanExportPDF.setDisable(true);
        }
    }
    /*----------------CRUD-------------------*/

    //affichage de la table Professeur
    public ObservableList<Professeur> ajoutProfesseurList() throws SQLException {
        ObservableList<Professeur> listData = FXCollections.observableArrayList();

        String sql = "SELECT * FROM Professeurs ORDER BY dateAjout";

        DBConnect dbConnect = new DBConnect();
        ResultSet resultSet = dbConnect.fetch_in_bdd(sql);

        Professeur professeurData;
        while (resultSet.next()) {
            professeurData = new Professeur(resultSet.getString("matricule"),
                    resultSet.getString("nom"),
                    resultSet.getInt("tauxHoraire"));

            listData.add(professeurData);
        }

        return listData;
    }

    public void ajoutProfAfficheList() throws SQLException {

        ProfesseurList = ajoutProfesseurList();

        matriculeProf.setCellValueFactory(new PropertyValueFactory<>("matriculeProf"));
        nomProf.setCellValueFactory(new PropertyValueFactory<>("nomProf"));
        tauxHoraireProf.setCellValueFactory(new PropertyValueFactory<>("tauxHoraire"));

        tableListProf.setItems(ProfesseurList);

        searchProfByMat.setSelected(true);
        searchProfByNom.setSelected(true);

        //Recherche item
        FilteredList<Professeur> filteredList = new FilteredList<>(ProfesseurList, b -> true);
        searchProf.textProperty().addListener((observable, oldValue, newValue) -> {
            System.err.println(filteredList);
            filteredList.setPredicate(Professeur -> {
                if (newValue.isEmpty() || newValue.isBlank() || newValue == null) return true;

                String searchKeyword = newValue.toLowerCase();

                if (!searchProfByNom.isSelected() && !searchProfByMat.isSelected()) return true;

                if (searchProfByMat.isSelected() && (Professeur.getMatriculeProf().toLowerCase().contains(searchKeyword)))
                    return true;
                return searchProfByNom.isSelected() && (Professeur.getNomProf().toLowerCase().contains(searchKeyword));
            });
        });

        SortedList<Professeur> sortedList = new SortedList<>(filteredList);

        sortedList.comparatorProperty().bind(tableListProf.comparatorProperty());

        tableListProf.setItems(sortedList);
    }

    //affichage de la table matieres
    public ObservableList<Matiere> ajoutMatiereList() throws SQLException {
        ObservableList<Matiere> listData = FXCollections.observableArrayList();

        String sql = "SELECT * FROM Matieres ORDER BY dateAjout";

        DBConnect dbConnect = new DBConnect();
        ResultSet resultSet = dbConnect.fetch_in_bdd(sql);

        Matiere matiereData;
        while (resultSet.next()) {
            matiereData = new Matiere(resultSet.getString("numMat"),
                    resultSet.getString("design"),
                    resultSet.getInt("nbrHeure"));

            listData.add(matiereData);
        }

        return listData;
    }

    //affichage du volume horaire
    public ObservableList<VH> ajoutVHList() throws SQLException {
        ObservableList<VH> listData = FXCollections.observableArrayList();

        String sql = "SELECT * FROM volumeHoraire ORDER BY dateAjout";

        DBConnect dbConnect = new DBConnect();
        try (ResultSet resultSet = dbConnect.fetch_in_bdd(sql)) {

            VH VHData;
            while (resultSet.next()) {
                VHData = new VH(
                        resultSet.getInt("annee"),
                        resultSet.getString("matricule"),
                        resultSet.getString("numMat")
                );

                listData.add(VHData);
            }
        }

        return listData;
    }

    /*-------------CRUD PROF---------------*/
    @FXML
    void AjouterProf(ActionEvent event) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("../View/ProfView/AddProf.fxml"));
        Parent parent = fxmlLoader.load();

        ProfController prof = fxmlLoader.getController();
        prof.requestParent(parent);
        prof.request(tableListProf, ProfesseurList, matriculeProf, nomProf, tauxHoraireProf, tableListeVH, VHList, matProfCol, numMatiereCol, anneeCol);

        Scene scene = new Scene(parent);

        Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.initOwner(primaryStage);
        stage.initStyle(StageStyle.UNDECORATED);
        stage.setScene(scene);

        FadeTransition ft = new FadeTransition(Duration.millis(1000), parent);
        ft.setFromValue(0.0);
        ft.setToValue(1.0);
        ft.play();

        stage.showAndWait();
    }

    @FXML
    void modifierProf(ActionEvent event) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("../View/ProfView/UpdateProf.fxml"));
        Parent parent = fxmlLoader.load();

        ProfController prof = fxmlLoader.getController();
        prof.requestParent(parent);
        prof.request(tableListProf, ProfesseurList, matriculeProf, nomProf, tauxHoraireProf, tableListeVH, VHList, matProfCol, numMatiereCol, anneeCol);

        TextField textField1 = (TextField) parent.lookup("#matricule");
        textField1.setText(profSel.getMatriculeProf());

        TextField textField2 = (TextField) parent.lookup("#nom");
        textField2.setText(profSel.getNomProf());

        TextField textField3 = (TextField) parent.lookup("#tauxHoraire");
        textField3.setText(String.valueOf(profSel.getTauxHoraire()));

        Scene scene = new Scene(parent);

        Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.initOwner(primaryStage);
        stage.initStyle(StageStyle.UNDECORATED);
        stage.setScene(scene);

        FadeTransition ft = new FadeTransition(Duration.millis(1000), parent);
        ft.setFromValue(0.0);
        ft.setToValue(1.0);
        ft.play();

        stage.showAndWait();
    }

    @FXML
    void supprimerProf(ActionEvent event) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("../View/ProfView/DeleteProf.fxml"));
        Parent parent = fxmlLoader.load();

        ProfController prof = fxmlLoader.getController();
        prof.requestParent(parent);
        prof.request(tableListProf, ProfesseurList, matriculeProf, nomProf, tauxHoraireProf, tableListeVH, VHList, matProfCol, numMatiereCol, anneeCol);

        TextField textField1 = (TextField) parent.lookup("#matricule");
        textField1.setText(profSel.getMatriculeProf());

        TextField textField2 = (TextField) parent.lookup("#nom");
        textField2.setText(profSel.getNomProf());

        TextField textField3 = (TextField) parent.lookup("#tauxHoraire");
        textField3.setText(String.valueOf(profSel.getTauxHoraire()));

        Scene scene = new Scene(parent);

        Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.initOwner(primaryStage);
        stage.initStyle(StageStyle.UNDECORATED);
        stage.setScene(scene);

        FadeTransition ft = new FadeTransition(Duration.millis(1000), parent);
        ft.setFromValue(0.0);
        ft.setToValue(1.0);
        ft.play();

        stage.showAndWait();
    }

    @FXML
    void creerBulletinPaieProfesseur(ActionEvent event) throws Exception {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("../View/ProfView/FicheProf.fxml"));
        Parent parent = fxmlLoader.load();

        ProfController prof = fxmlLoader.getController();
        prof.requestParent(parent);
        prof.request(tableListProf, ProfesseurList, matriculeProf, nomProf, tauxHoraireProf, tableListeVH, VHList, matProfCol, numMatiereCol, anneeCol);

        List<String> allA = new ArrayList<String>();

        ChoiceBox choiceBox = (ChoiceBox) parent.lookup("#matriculeEx");
        ChoiceBox choiceBox1 = (ChoiceBox) parent.lookup("#nomP");
        ChoiceBox choiceBox2 = (ChoiceBox) parent.lookup("#anneeVH");

        DBConnect dbConnect = new DBConnect();
        ResultSet resultSet1 = dbConnect.fetch_in_bdd("SELECT DISTINCT annee FROM volumeHoraire WHERE matricule = '" + profSel.getMatriculeProf() + "';");

        while (resultSet1.next()) {
            allA.add(String.valueOf(resultSet1.getInt("annee")));
        }

        choiceBox.setValue(profSel.getMatriculeProf());
        choiceBox1.setValue(profSel.getNomProf());
        choiceBox2.setValue(allA.get(0));
        ObservableList<String> listA = FXCollections.observableArrayList(allA);
        choiceBox2.setItems(listA);

        Scene scene = new Scene(parent);

        Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.initOwner(primaryStage);
        stage.initStyle(StageStyle.UNDECORATED);
        stage.setScene(scene);

        FadeTransition ft = new FadeTransition(Duration.millis(1000), parent);
        ft.setFromValue(0.0);
        ft.setToValue(1.0);
        ft.play();

        stage.showAndWait();
    }
    /*------------CRUD MATIERES--------------*/

    /*-------------CRUD PROF---------------*/
    /*------------CRUD MATIERES--------------*/
    @FXML
    void AjoutMatAction(ActionEvent event) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("../View/MatView/AddMat.fxml"));
        Parent parent = fxmlLoader.load();

        MatController mat = fxmlLoader.getController();
        mat.requestParent(parent);
        mat.request(tableListMat, MatiereList, numMatCol, designMatCol, nbrHeureMatCol);

        Scene scene = new Scene(parent);

        Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.initOwner(primaryStage);
        stage.initStyle(StageStyle.UNDECORATED);
        stage.setScene(scene);

        FadeTransition ft = new FadeTransition(Duration.millis(1000), parent);
        ft.setFromValue(0.0);
        ft.setToValue(1.0);
        ft.play();

        stage.showAndWait();
    }

    @FXML
    void modifierMatAction(ActionEvent event) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("../View/MatView/UpdateMat.fxml"));
        Parent parent = fxmlLoader.load();

        MatController mat = fxmlLoader.getController();
        mat.requestParent(parent);
        mat.request(tableListMat, MatiereList, numMatCol, designMatCol, nbrHeureMatCol);

        TextField textField1 = (TextField) parent.lookup("#numMat");
        textField1.setText(matSel.getNumMat());

        TextField textField2 = (TextField) parent.lookup("#designMat");
        textField2.setText(matSel.getDesignMat());

        TextField textField3 = (TextField) parent.lookup("#nbrHMat");
        textField3.setText(String.valueOf(matSel.getNbrHeureMat()));

        Scene scene = new Scene(parent);

        Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.initOwner(primaryStage);
        stage.initStyle(StageStyle.UNDECORATED);
        stage.setScene(scene);

        FadeTransition ft = new FadeTransition(Duration.millis(1000), parent);
        ft.setFromValue(0.0);
        ft.setToValue(1.0);
        ft.play();

        stage.showAndWait();
    }

    @FXML
    void supMatAction(ActionEvent event) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("../View/MatView/DeleteMat.fxml"));
        Parent parent = fxmlLoader.load();

        MatController mat = fxmlLoader.getController();
        mat.requestParent(parent);
        mat.request(tableListMat, MatiereList, numMatCol, designMatCol, nbrHeureMatCol);

        TextField textField1 = (TextField) parent.lookup("#numMat");
        textField1.setText(matSel.getNumMat());

        TextField textField2 = (TextField) parent.lookup("#designMat");
        textField2.setText(matSel.getDesignMat());

        TextField textField3 = (TextField) parent.lookup("#nbrHMat");
        textField3.setText(String.valueOf(matSel.getNbrHeureMat()));

        Scene scene = new Scene(parent);

        Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.initOwner(primaryStage);
        stage.initStyle(StageStyle.UNDECORATED);
        stage.setScene(scene);

        FadeTransition ft = new FadeTransition(Duration.millis(1000), parent);
        ft.setFromValue(0.0);
        ft.setToValue(1.0);
        ft.play();

        stage.showAndWait();
    }

    @FXML
    void exportMatAction(ActionEvent event) {

    }
    /*------------CRUD VOLUME HORAIRE--------------*/

    /*----------------CRUD-------------------*/

    /*------------CRUD VOLUME HORAIRE--------------*/
    @FXML
    void ajouterVolHAction(ActionEvent actionEvent) throws IOException {
        //System.out.println(numMNA);
        if (tableListProf.getItems().size() < 1) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Erreur");
            alert.setHeaderText(null);
            alert.setContentText("Liste des professeurs vide!!\nVeuiller en ajouter pour continuer!");
            alert.showAndWait();
        } else {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("../View/VHView/AddVH.fxml"));
            Parent parent = fxmlLoader.load();

            VHController vh = fxmlLoader.getController();
            vh.requestParent(parent);
            vh.request(tableListeVH, VHList, matProfCol, numMatiereCol, anneeCol);

            Scene scene = new Scene(parent);

            Stage stage = new Stage();
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.initOwner(primaryStage);
            stage.initStyle(StageStyle.UNDECORATED);
            stage.setScene(scene);

            FadeTransition ft = new FadeTransition(Duration.millis(1000), parent);
            ft.setFromValue(0.0);
            ft.setToValue(1.0);
            ft.play();

            stage.showAndWait();
        }
    }

    /*----------------Menu-------------------*/

    @FXML
    void modifierVolHAction(ActionEvent actionEvent) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("../View/VHView/UpdateVH.fxml"));
        Parent parent = fxmlLoader.load();

        VHController vh = fxmlLoader.getController();
        vh.requestParent(parent);

        vh.request(tableListeVH, VHList, matProfCol, numMatiereCol, anneeCol);

        ChoiceBox choiceBox1 = (ChoiceBox) parent.lookup("#nomProf");
        choiceBox1.setValue(vhSel.getMatProf());

        ChoiceBox choiceBox2 = (ChoiceBox) parent.lookup("#designMat");
        choiceBox2.setValue(vhSel.getNumMatiere());

        TextField textField = (TextField) parent.lookup("#annee");
        textField.setText(String.valueOf(vhSel.getAnneeVH()));


        vh.setEditVal(vhSel.getMatProf(), vhSel.getNumMatiere(), String.valueOf(vhSel.getAnneeVH()));

        Scene scene = new Scene(parent);

        Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.initOwner(primaryStage);
        stage.initStyle(StageStyle.UNDECORATED);
        stage.setScene(scene);

        FadeTransition ft = new FadeTransition(Duration.millis(1000), parent);
        ft.setFromValue(0.0);
        ft.setToValue(1.0);
        ft.play();

        stage.showAndWait();
    }

    @FXML
    void supVolHAction(ActionEvent actionEvent) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("../View/VHView/DeleteVH.fxml"));
        Parent parent = fxmlLoader.load();

        VHController vh = fxmlLoader.getController();
        vh.requestParent(parent);
        vh.request(tableListeVH, VHList, matProfCol, numMatiereCol, anneeCol);

        ChoiceBox choiceBox1 = (ChoiceBox) parent.lookup("#nomProf");
        choiceBox1.setValue(vhSel.getMatProf());

        ChoiceBox choiceBox2 = (ChoiceBox) parent.lookup("#designMat");
        choiceBox2.setValue(vhSel.getNumMatiere());

        TextField textField = (TextField) parent.lookup("#annee");
        textField.setText(String.valueOf(vhSel.getAnneeVH()));

        Scene scene = new Scene(parent);

        Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.initOwner(primaryStage);
        stage.initStyle(StageStyle.UNDECORATED);
        stage.setScene(scene);

        FadeTransition ft = new FadeTransition(Duration.millis(1000), parent);
        ft.setFromValue(0.0);
        ft.setToValue(1.0);
        ft.play();

        stage.showAndWait();
    }

    @FXML
    void exporterPDFVolHAction(ActionEvent actionEvent) {

    }

    /*----------------Menu-------------------*/
    @FXML
    void switch_form(ActionEvent actionEvent) throws SQLException {
        //System.out.println("switch_form function worked fine!!!");
        if (actionEvent.getSource() == menuItemDashboard) {
            menuItemDashboard.setStyle("-fx-background-color: #FFFFFF;");
            menuItemDashboard.setOnMouseEntered(event -> menuItemDashboard.setStyle("-fx-background-color: #BFCDE1;"));
            menuItemDashboard.setOnMouseExited(event -> menuItemDashboard.setStyle("-fx-background-color: #FFFFFF;"));
            menuItemDashboard.setOnMousePressed(event -> menuItemDashboard.setStyle("-fx-background-color: #EBE8F9;"));

            menuItemMat.setStyle("-fx-background-color: #EBE8F9;");
            menuItemMat.setOnMouseEntered(event -> menuItemMat.setStyle("-fx-background-color: #BFCDE1;"));
            menuItemMat.setOnMouseExited(event -> menuItemMat.setStyle("-fx-background-color: #EBE8F9;"));
            menuItemMat.setOnMousePressed(event -> menuItemMat.setStyle("-fx-background-color: #EBE8F9;"));

            menuItemProf.setStyle("-fx-background-color: #EBE8F9;");
            menuItemProf.setOnMouseEntered(event -> menuItemProf.setStyle("-fx-background-color: #BFCDE1;"));
            menuItemProf.setOnMouseExited(event -> menuItemProf.setStyle("-fx-background-color: #EBE8F9;"));
            menuItemProf.setOnMousePressed(event -> menuItemProf.setStyle("-fx-background-color: #EBE8F9;"));

            menuItemVolH.setStyle("-fx-background-color: #EBE8F9;");
            menuItemVolH.setOnMouseEntered(event -> menuItemVolH.setStyle("-fx-background-color: #BFCDE1;"));
            menuItemVolH.setOnMouseExited(event -> menuItemVolH.setStyle("-fx-background-color: #EBE8F9;"));
            menuItemVolH.setOnMousePressed(event -> menuItemVolH.setStyle("-fx-background-color: #EBE8F9;"));

            homePane.setVisible(true);
            profPane.setVisible(false);
            matPane.setVisible(false);
            volHPane.setVisible(false);


            //Recuperation et affichage du compte de prof dans main
            Label label = (Label) mainPane.lookup("#profCount");
            DBConnect dbConnect = new DBConnect();

            String query = "SELECT * FROM Professeurs";
            ResultSet res = null;

            res = dbConnect.fetch_in_bdd(query);

            res.last();
            label.setText(String.valueOf(res.getRow()));

            //Recuperation et affichage du compte de mat dans main
            Label label1 = (Label) mainPane.lookup("#matCount");

            String query1 = "SELECT * FROM Matieres";
            ResultSet res1 = null;

            res1 = dbConnect.fetch_in_bdd(query1);

            res1.last();
            label1.setText(String.valueOf(res1.getRow()));

            //Recuperation et affichage du compte de volH dans main
            Label label2 = (Label) mainPane.lookup("#volHCount");

            String query2 = "SELECT * FROM volumeHoraire";
            ResultSet res2 = null;

            res2 = dbConnect.fetch_in_bdd(query2);

            res2.last();
            label2.setText(String.valueOf(res2.getRow()));

            String reqAn = "SELECT DISTINCT annee FROM volumeHoraire ORDER BY annee ASC;";
            ResultSet resultSet = null;
            List<String> listAnnB = new ArrayList<>();
            try {
                resultSet = dbConnect.fetch_in_bdd(reqAn);
                anneeBilan.setItems(FXCollections.observableArrayList(listAnnB));
                anneeBilan.setValue(null);
                anneeA = 0;
                while (resultSet.next()) {
                    listAnnB.add(String.valueOf(resultSet.getInt("annee")));
                    anneeBilan.setItems(FXCollections.observableArrayList(listAnnB));
                    anneeBilan.setValue(listAnnB.get(0));
                    anneeA = parseInt(listAnnB.get(0));
                }
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
            chargerDonneesEnTempsReel();

        } else if (actionEvent.getSource() == menuItemProf) {
            if (profSel != null) {
                DBConnect dbConnect = new DBConnect();
                try {
                    ResultSet resultSet1 = dbConnect.fetch_in_bdd("SELECT COUNT(*) FROM volumeHoraire WHERE matricule = '" + profSel.getMatriculeProf() + "';");
                    resultSet1.next();
                    BulletinPaieProf.setDisable(resultSet1.getInt(1) <= 0);
                } catch (SQLException e) {
                    throw new RuntimeException(e);
                }
            }
            menuItemDashboard.setStyle("-fx-background-color: #EBE8F9;");
            menuItemDashboard.setOnMouseEntered(event -> menuItemDashboard.setStyle("-fx-background-color: #BFCDE1;"));
            menuItemDashboard.setOnMouseExited(event -> menuItemDashboard.setStyle("-fx-background-color: #EBE8F9;"));
            menuItemDashboard.setOnMousePressed(event -> menuItemDashboard.setStyle("-fx-background-color: #EBE8F9;"));

            menuItemMat.setStyle("-fx-background-color: #EBE8F9;");
            menuItemMat.setOnMouseEntered(event -> menuItemMat.setStyle("-fx-background-color: #BFCDE1;"));
            menuItemMat.setOnMouseExited(event -> menuItemMat.setStyle("-fx-background-color: #EBE8F9;"));
            menuItemMat.setOnMousePressed(event -> menuItemMat.setStyle("-fx-background-color: #EBE8F9;"));

            menuItemProf.setStyle("-fx-background-color: #FFFFFF;");
            menuItemProf.setOnMouseEntered(event -> menuItemProf.setStyle("-fx-background-color: #BFCDE1;"));
            menuItemProf.setOnMouseExited(event -> menuItemProf.setStyle("-fx-background-color: #FFFFFF;"));
            menuItemProf.setOnMousePressed(event -> menuItemProf.setStyle("-fx-background-color: #EBE8F9;"));

            menuItemVolH.setStyle("-fx-background-color: #EBE8F9;");
            menuItemVolH.setOnMouseEntered(event -> menuItemVolH.setStyle("-fx-background-color: #BFCDE1;"));
            menuItemVolH.setOnMouseExited(event -> menuItemVolH.setStyle("-fx-background-color: #EBE8F9;"));
            menuItemVolH.setOnMousePressed(event -> menuItemVolH.setStyle("-fx-background-color: #EBE8F9;"));

            homePane.setVisible(false);
            profPane.setVisible(true);
            matPane.setVisible(false);
            volHPane.setVisible(false);
        } else if (actionEvent.getSource() == menuItemMat) {
            menuItemDashboard.setStyle("-fx-background-color: #EBE8F9;");
            menuItemDashboard.setOnMouseEntered(event -> menuItemDashboard.setStyle("-fx-background-color: #BFCDE1;"));
            menuItemDashboard.setOnMouseExited(event -> menuItemDashboard.setStyle("-fx-background-color: #EBE8F9;"));
            menuItemDashboard.setOnMousePressed(event -> menuItemDashboard.setStyle("-fx-background-color: #EBE8F9;"));

            menuItemMat.setStyle("-fx-background-color: #FFFFFF;");
            menuItemMat.setOnMouseEntered(event -> menuItemMat.setStyle("-fx-background-color: #BFCDE1;"));
            menuItemMat.setOnMouseExited(event -> menuItemMat.setStyle("-fx-background-color: #FFFFFF;"));
            menuItemMat.setOnMousePressed(event -> menuItemMat.setStyle("-fx-background-color: #EBE8F9;"));

            menuItemProf.setStyle("-fx-background-color: #EBE8F9;");
            menuItemProf.setOnMouseEntered(event -> menuItemProf.setStyle("-fx-background-color: #BFCDE1;"));
            menuItemProf.setOnMouseExited(event -> menuItemProf.setStyle("-fx-background-color: #EBE8F9;"));
            menuItemProf.setOnMousePressed(event -> menuItemProf.setStyle("-fx-background-color: #EBE8F9;"));

            menuItemVolH.setStyle("-fx-background-color: #EBE8F9;");
            menuItemVolH.setOnMouseEntered(event -> menuItemVolH.setStyle("-fx-background-color: #BFCDE1;"));
            menuItemVolH.setOnMouseExited(event -> menuItemVolH.setStyle("-fx-background-color: #EBE8F9;"));
            menuItemVolH.setOnMousePressed(event -> menuItemVolH.setStyle("-fx-background-color: #EBE8F9;"));

            homePane.setVisible(false);
            profPane.setVisible(false);
            matPane.setVisible(true);
            volHPane.setVisible(false);
        } else if (actionEvent.getSource() == menuItemVolH) {
            DBConnect dbConnect = new DBConnect();
            ResultSet resultSet = dbConnect.fetch_in_bdd("SELECT count(*) AS countMat FROM Matieres");
            resultSet.next();
            if (resultSet.getInt("countMat") == 0) {
                supVolH.setDisable(true);
                modifierVolH.setDisable(true);
                ajouterVolH.setDisable(true);
            } else {
                ajouterVolH.setDisable(false);
            }

            menuItemDashboard.setStyle("-fx-background-color: #EBE8F9;");
            menuItemDashboard.setOnMouseEntered(event -> menuItemDashboard.setStyle("-fx-background-color: #BFCDE1;"));
            menuItemDashboard.setOnMouseExited(event -> menuItemDashboard.setStyle("-fx-background-color: #EBE8F9;"));
            menuItemDashboard.setOnMousePressed(event -> menuItemDashboard.setStyle("-fx-background-color: #EBE8F9;"));

            menuItemMat.setStyle("-fx-background-color: #EBE8F9;");
            menuItemMat.setOnMouseEntered(event -> menuItemMat.setStyle("-fx-background-color: #BFCDE1;"));
            menuItemMat.setOnMouseExited(event -> menuItemMat.setStyle("-fx-background-color: #EBE8F9;"));
            menuItemMat.setOnMousePressed(event -> menuItemMat.setStyle("-fx-background-color: #EBE8F9;"));

            menuItemProf.setStyle("-fx-background-color: #EBE8F9;");
            menuItemProf.setOnMouseEntered(event -> menuItemProf.setStyle("-fx-background-color: #BFCDE1;"));
            menuItemProf.setOnMouseExited(event -> menuItemProf.setStyle("-fx-background-color: #EBE8F9;"));
            menuItemProf.setOnMousePressed(event -> menuItemProf.setStyle("-fx-background-color: #EBE8F9;"));

            menuItemVolH.setStyle("-fx-background-color: #FFFFFF;");
            menuItemVolH.setOnMouseEntered(event -> menuItemVolH.setStyle("-fx-background-color: #BFCDE1;"));
            menuItemVolH.setOnMouseExited(event -> menuItemVolH.setStyle("-fx-background-color: #FFFFFF;"));
            menuItemVolH.setOnMousePressed(event -> menuItemVolH.setStyle("-fx-background-color: #EBE8F9;"));


            homePane.setVisible(false);
            profPane.setVisible(false);
            matPane.setVisible(false);
            volHPane.setVisible(true);
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        chargerDonneesEnTempsReel();
        try {
            ajoutProfAfficheList();
            ajoutMatiereAfficheList();
            ajoutVHAfficheList();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        //Style du menu par defaut
        menuItemDashboard.setStyle("-fx-background-color: #FFFFFF;");
        menuItemDashboard.setOnMouseEntered(event -> menuItemDashboard.setStyle("-fx-background-color: #BFCDE1;"));
        menuItemDashboard.setOnMouseExited(event -> menuItemDashboard.setStyle("-fx-background-color: #FFFFFF;"));
        menuItemDashboard.setOnMousePressed(event -> menuItemDashboard.setStyle("-fx-background-color: #EBE8F9;"));

        menuItemMat.setStyle("-fx-background-color: #EBE8F9;");
        menuItemMat.setOnMouseEntered(event -> menuItemMat.setStyle("-fx-background-color: #BFCDE1;"));
        menuItemMat.setOnMouseExited(event -> menuItemMat.setStyle("-fx-background-color: #EBE8F9;"));
        menuItemMat.setOnMousePressed(event -> menuItemMat.setStyle("-fx-background-color: #EBE8F9;"));

        menuItemProf.setStyle("-fx-background-color: #EBE8F9;");
        menuItemProf.setOnMouseEntered(event -> menuItemProf.setStyle("-fx-background-color: #BFCDE1;"));
        menuItemProf.setOnMouseExited(event -> menuItemProf.setStyle("-fx-background-color: #EBE8F9;"));
        menuItemProf.setOnMousePressed(event -> menuItemProf.setStyle("-fx-background-color: #EBE8F9;"));

        menuItemVolH.setStyle("-fx-background-color: #EBE8F9;");
        menuItemVolH.setOnMouseEntered(event -> menuItemVolH.setStyle("-fx-background-color: #BFCDE1;"));
        menuItemVolH.setOnMouseExited(event -> menuItemVolH.setStyle("-fx-background-color: #EBE8F9;"));
        menuItemVolH.setOnMousePressed(event -> menuItemVolH.setStyle("-fx-background-color: #EBE8F9;"));

        //--------------------Pane Professeur---------------------//
        ModifierProf.setDisable(true);
        // Ajout de l'écouteur pour activer le bouton modifier lorsque qu'une ligne est sélectionnée
        tableListProf.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Professeur>() {
            @Override
            public void changed(ObservableValue<? extends Professeur> observable, Professeur oldValue, Professeur newValue) {
                ModifierProf.setDisable(newValue == null);
                profSel = newValue;
            }
        });

        SupProf.setDisable(true);
        // Ajout de l'écouteur pour activer le bouton supprimer lorsque qu'une ligne est sélectionnée
        tableListProf.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Professeur>() {
            @Override
            public void changed(ObservableValue<? extends Professeur> observable, Professeur oldValue, Professeur newValue) {
                SupProf.setDisable(newValue == null);
                profSel = newValue;
            }
        });

        BulletinPaieProf.setDisable(true);
        // Ajout de l'écouteur pour activer le bouton pdfGenerator lorsque qu'une ligne est sélectionnée
        tableListProf.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Professeur>() {
            @Override
            public void changed(ObservableValue<? extends Professeur> observable, Professeur oldValue, Professeur newValue) {

                if (newValue != null) {
                    DBConnect dbConnect = new DBConnect();
                    try {
                        ResultSet resultSet1 = dbConnect.fetch_in_bdd("SELECT COUNT(*) FROM volumeHoraire WHERE matricule = '" + newValue.getMatriculeProf() + "';");
                        resultSet1.next();
                        BulletinPaieProf.setDisable(resultSet1.getInt(1) <= 0);
                    } catch (SQLException e) {
                        throw new RuntimeException(e);
                    }
                } else {
                    BulletinPaieProf.setDisable(true);
                }
                profSel = newValue;
            }
        });
        //--------------------Professeurs Pane-------------------------//

        //--------------------Matieres Pane-------------------------//
        modifierMat.setDisable(true);
        // Ajout de l'écouteur pour activer le bouton modifier lorsque qu'une ligne est sélectionnée
        tableListMat.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Matiere>() {
            @Override
            public void changed(ObservableValue<? extends Matiere> observable, Matiere oldValue, Matiere newValue) {
                modifierMat.setDisable(newValue == null);
                matSel = newValue;
            }
        });

        supMat.setDisable(true);
        // Ajout de l'écouteur pour activer le bouton supprimer lorsque qu'une ligne est sélectionnée
        tableListMat.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Matiere>() {
            @Override
            public void changed(ObservableValue<? extends Matiere> observable, Matiere oldValue, Matiere newValue) {
                supMat.setDisable(newValue == null);
                matSel = newValue;
            }
        });

        //--------------------Matieres Pane-------------------------//
        //--------------------Volume horaire Pane-------------------------//
        modifierVolH.setDisable(true);
        // Ajout de l'écouteur pour activer le bouton modifier lorsque qu'une ligne est sélectionnée
        tableListeVH.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<VH>() {
            @Override
            public void changed(ObservableValue<? extends VH> observable, VH oldValue, VH newValue) {
                modifierVolH.setDisable(newValue == null);
                vhSel = newValue;
            }
        });

        supVolH.setDisable(true);
        // Ajout de l'écouteur pour activer le bouton supprimer lorsque qu'une ligne est sélectionnée
        tableListeVH.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<VH>() {
            @Override
            public void changed(ObservableValue<? extends VH> observable, VH oldValue, VH newValue) {
                supVolH.setDisable(newValue == null);
                vhSel = newValue;
            }
        });
        //--------------------Volume horaire Pane-------------------------//

    }

    /*---------------------------window button-------------------------------*/
    @FXML
    void minimApp(ActionEvent event) {
        Stage stage = (Stage) mainPane.getScene().getWindow();
        stage.setIconified(true);
    }

    public void closeApp(ActionEvent actionEvent) {
        Stage stage = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();
        FadeTransition ft = new FadeTransition(Duration.millis(1000), stage.getScene().getRoot());
        ft.setFromValue(1.0);
        ft.setToValue(0.0);
        ft.setOnFinished(event -> stage.close());
        ft.play();
    }

    //Exporter le bilan sous forme pdf
    public void bilanExportPDF(ActionEvent actionEvent) throws IOException, DocumentException, SQLException {
        DBConnect dbConnect = new DBConnect();
        String sql = "SELECT\n" +
                "  p.matricule,\n" +
                "  p.nom,\n" +
                "  SUM(m.nbrHeure * p.tauxHoraire) AS montant,\n" +
                " m.nbrHeure\n" +
                "FROM\n" +
                "  Professeurs p\n" +
                "  JOIN volumeHoraire vh ON p.matricule = vh.matricule\n" +
                "  JOIN Matieres m ON vh.numMat = m.numMat\n" +
                "WHERE annee = " + anneeA + "\n" +
                "GROUP BY p.matricule;";
        ResultSet resultSet = dbConnect.fetch_in_bdd(sql);

        Document document = new Document();

        PdfWriter.getInstance(document, new FileOutputStream("VH_" + anneeA + ".pdf"));
        document.open();

        Rectangle pageSize = document.getPageSize();
        float pageWidth = pageSize.getWidth();

        float padding = pageWidth / 40;

        Font titreFont = FontFactory.getFont(FontFactory.HELVETICA, 24, Font.BOLD, new BaseColor(255, 255, 255));
        PdfPCell titreCell = new PdfPCell(new Phrase("HEURE COMPLÉMENTAIRE DANS UN AN", titreFont));
        titreCell.setBackgroundColor(new BaseColor(42, 115, 255));
        titreCell.setPadding(padding);
        titreCell.setBorder(Rectangle.NO_BORDER);
        titreCell.setHorizontalAlignment(Element.ALIGN_CENTER);

        PdfPTable table = new PdfPTable(1);
        table.setWidthPercentage(100);
        table.addCell(titreCell);
        table.setSpacingAfter(10);

        document.add(table);


        PdfPTable annee = new PdfPTable(1);
        annee.setWidthPercentage(100);
        annee.addCell(createCell("Année          :     " + anneeA, false, false));

        annee.setSpacingAfter(15);

        PdfPTable tableau = new PdfPTable(3);
        tableau.setWidthPercentage(100);
        tableau.addCell(createCell("Matricule", true));
        tableau.addCell(createCell("Nom", true));
        tableau.addCell(createCell("Montant", true));

        document.add(titreCell);
        document.add(annee);
        document.add(tableau);

        int montTotal = 0;
        while (resultSet.next()) {
            PdfPTable rowI = new PdfPTable(3);
            rowI.setWidthPercentage(100);
            rowI.addCell(createCell(resultSet.getString("p.matricule"), false));
            rowI.addCell(createCell(resultSet.getString("p.nom"), false));
            rowI.addCell(createCell(resultSet.getString("montant"), false));

            montTotal += resultSet.getInt("montant");
            document.add(rowI);
        }
        PdfPTable rowTotal = new PdfPTable(2);
        rowTotal.setWidthPercentage((float) 200 / 3);
        rowTotal.setHorizontalAlignment(Element.ALIGN_RIGHT);
        rowTotal.addCell(createCell("Total", false));
        rowTotal.addCell(createCell(montTotal + " Ar", false));

        document.add(rowTotal);

        document.close();
        String command = "open VH_" + anneeA + ".pdf";

        ProcessBuilder processBuilder = new ProcessBuilder("bash", "-c", command);

        processBuilder.redirectErrorStream(true);

        try {
            processBuilder.start();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Succès");
        alert.setHeaderText(null);
        alert.setContentText("Fiche de paie générée avec succès !");

        Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
        stage.setAlwaysOnTop(true);

        alert.showAndWait();
    }
}
