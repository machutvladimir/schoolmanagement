package Controller;

import Model.Matiere;
import javafx.animation.FadeTransition;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.BooleanBinding;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Duration;
import required.DBConnect;

import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class MatController implements Initializable {
    private final List<String> allPrimKey = new ArrayList<>();
    @FXML
    Parent parent;
    @FXML
    Button send;
    private TableView<Matiere> tableListMat;
    private ObservableList<Matiere> matiereList;
    private TableColumn<Matiere, String> numMatCol;
    private TableColumn<Matiere, String> designMatCol;
    private TableColumn<Matiere, Integer> nbrHeureMatCol;
    @FXML
    private TextField numMatAdd;
    @FXML
    private TextField designMat;
    @FXML
    private TextField nbrHMat;

    public void requestParent(Parent parent) {
        this.parent = parent;
    }

    public void request(TableView<Matiere> tableListMat, ObservableList<Matiere> matiereList, TableColumn<Matiere, String> numMatCol, TableColumn<Matiere, String> designMatCol, TableColumn<Matiere, Integer> nbrHeureMatCol) {
        this.tableListMat = tableListMat;
        this.matiereList = matiereList;
        this.numMatCol = numMatCol;
        this.designMatCol = designMatCol;
        this.nbrHeureMatCol = nbrHeureMatCol;
    }

    /*--------------Les bouttons dans popup---------------*/
    //Envoie de donnee dans ajout
    public void send(ActionEvent actionEvent) throws SQLException {
        DBConnect dbConnect = new DBConnect();
        String query = "INSERT INTO Matieres (numMat, design, nbrHeure) VALUES (?,?,?)";
        String numMatVal = "";
        String designMatVal = "";
        int nbrHVal = 0;

        TextField textField1 = (TextField) parent.lookup("#numMatAdd");
        numMatVal = textField1.getText();

        TextField textField2 = (TextField) parent.lookup("#designMat");
        designMatVal = textField2.getText();

        TextField textField3 = (TextField) parent.lookup("#nbrHMat");
        nbrHVal = Integer.parseInt(textField3.getText());

        int sucAjout = dbConnect.edit_bdd(query, numMatVal, designMatVal, nbrHVal);

        if (sucAjout == 1) {
            closePopup(actionEvent);
            alertMatSuccess();
            ajoutMatiereAfficheList();
        } else {
            alertMatError();
        }

    }

    public void sendUpdate(ActionEvent actionEvent) throws SQLException {
        String numMatVal = "";
        String designMatVal = "";
        int nbrHVal = 0;
        String query = "UPDATE `Matieres` SET `design`=?,`nbrHeure`=? WHERE numMat = ?";

        TextField textField1 = (TextField) parent.lookup("#numMat");
        numMatVal = textField1.getText();

        TextField textField2 = (TextField) parent.lookup("#designMat");
        designMatVal = textField2.getText();

        TextField textField3 = (TextField) parent.lookup("#nbrHMat");
        nbrHVal = Integer.parseInt(textField3.getText());


        DBConnect dbConnect = new DBConnect();
        dbConnect.fkCheck("SET FOREIGN_KEY_CHECKS=0;");
        if (dbConnect.edit_bdd(query, designMatVal, nbrHVal, numMatVal) == 0) alertMatError();
        else {
            ajoutMatiereAfficheList();
            closePopup(actionEvent);
            alertMatSuccess();
        }
        dbConnect.fkCheck("SET FOREIGN_KEY_CHECKS=1;");
    }

    public void removeItem(ActionEvent actionEvent) throws SQLException {
        DBConnect dbConnect = new DBConnect();
        String query = "DELETE FROM Matieres WHERE numMat=?";
        String queryForeign = "DELETE FROM volumeHoraire WHERE numMat=?";
        String numMatVal = "";


        TextField textField1 = (TextField) parent.lookup("#numMat");
        numMatVal = textField1.getText();

        dbConnect.fkCheck("SET FOREIGN_KEY_CHECKS=0;");
        dbConnect.removeItemFromBdd(queryForeign, numMatVal);
        dbConnect.fkCheck("SET FOREIGN_KEY_CHECKS=1;");

        if (dbConnect.removeItemFromBdd(query, numMatVal) == 0) {
            alertMatError();
        } else {
            ajoutMatiereAfficheList();
            alertMatSuccess();
        }
        closePopup(actionEvent);
    }
    /*-----------------------Validate input----------------------------------------*/

    @FXML
    private void closePopup(ActionEvent event) {
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        FadeTransition ft = new FadeTransition(Duration.millis(1000), stage.getScene().getRoot());
        ft.setFromValue(1.0);
        ft.setToValue(0.0);
        ft.setOnFinished(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                stage.close();
            }
        });
        ft.play();
    }

    void alertMatSuccess() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Succès");
        alert.setHeaderText(null);
        alert.initModality(Modality.APPLICATION_MODAL);
        alert.setContentText("L'opération a été effectuée avec succès !");
        alert.showAndWait();
    }

    void alertMatError() {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Erreur");
        alert.setHeaderText(null);
        alert.setContentText("Une erreur est survenu!\nVeuiller réessayer à nouveau s'il vous plaît!");
        alert.showAndWait();
    }

    public boolean validateNumMat(String st) {
        boolean b = false;
        for (String primkey : allPrimKey) b = b | (primkey.equalsIgnoreCase(st));
        // Le numMat est vide ou nul
        return !st.trim().isEmpty() && st.trim().matches("[a-zA-Z0-9_]+") && !b;
    }

    public boolean validateDesignMat(String st) {
        // Le DesignMat est vide ou nul
        return !st.trim().isEmpty() && st.trim().matches("[a-z A-Z]+");
    }

    public boolean validateNbrHMat(String st) {
        // Le nbrHMat est vide ou nul
        return !st.trim().isEmpty() && st.trim().matches("[0-9]+");
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        //Recuperation de toute les cles primaire "numMat" dans le bdd vers une variable liste array "allPrimKey"
        DBConnect dbConnect = new DBConnect();
        String sql = "SELECT numMat FROM Matieres";

        try {
            ResultSet resultSet = dbConnect.fetch_in_bdd(sql);
            while (resultSet.next()) {
                allPrimKey.add(resultSet.getString("numMat"));
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }


        if (numMatAdd != null)
            numMatAdd.textProperty().addListener((observable, oldValue, newValue) -> {
                boolean b = false;
                for (String primkey : allPrimKey) {
                    b |= (primkey.equals(newValue));
                }
                //System.out.println("12".equals("12") +"\n");
                if (newValue.trim().isEmpty() || !newValue.trim().matches("[a-zA-Z0-9_]+") || b) {
                    numMatAdd.setStyle("-fx-border-color: red ; -fx-border-width: 2px ;");
                } else {
                    numMatAdd.setStyle("");
                }
            });

        designMat.textProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue.trim().isEmpty() || !newValue.trim().matches("[a-z A-Z]+")) {
                designMat.setStyle("-fx-border-color: red ; -fx-border-width: 2px ;");
            } else {
                designMat.setStyle("");
            }
        });

        nbrHMat.textProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue.trim().isEmpty() || !newValue.trim().matches("[0-9]+")) {
                nbrHMat.setStyle("-fx-border-color: red ; -fx-border-width: 2px ;");
            } else {
                nbrHMat.setStyle("");
            }
        });

        // Créer une expression binaire qui vérifie si les entrées sont valides ou non
        if (numMatAdd != null) {
            BooleanBinding inputsValid = Bindings.createBooleanBinding(() -> {

                boolean inputM = validateNumMat(numMatAdd.getText());
                boolean inputN = validateDesignMat(designMat.getText());
                boolean inputVH = validateNbrHMat(nbrHMat.getText());

                return inputM && inputN && inputVH;
            }, numMatAdd.textProperty(), designMat.textProperty(), nbrHMat.textProperty());
            // Lier la propriété "disable" du bouton à l'expression d'entrées valides
            send.disableProperty().bind(inputsValid.not());
        } else {
            BooleanBinding inputsValid = Bindings.createBooleanBinding(() -> {
                boolean inputN = validateDesignMat(designMat.getText());
                boolean inputVH = validateNbrHMat(nbrHMat.getText());

                return inputN && inputVH;
            }, designMat.textProperty(), nbrHMat.textProperty());
            // Lier la propriété "disable" du bouton à l'expression d'entrées valides
            send.disableProperty().bind(inputsValid.not());
        }
    }


    /*--------------------------RELOAD MAT VIEW TABLE-----------------------------*/

    public ObservableList<Matiere> ajoutMatiereList() throws SQLException {
        ObservableList<Matiere> listData = FXCollections.observableArrayList();

        String sql = "SELECT * FROM Matieres ORDER BY dateAjout";

        DBConnect dbConnect = new DBConnect();
        ResultSet resultSet = dbConnect.fetch_in_bdd(sql);

        Matiere matiereData;
        while (resultSet.next()) {
            matiereData = new Matiere(resultSet.getString("numMat"),
                    resultSet.getString("design"),
                    resultSet.getInt("nbrHeure"));

            listData.add(matiereData);
        }

        return listData;
    }


    public void ajoutMatiereAfficheList() throws SQLException {
        matiereList = ajoutMatiereList();

        numMatCol.setCellValueFactory(new PropertyValueFactory<>("numMat"));
        designMatCol.setCellValueFactory(new PropertyValueFactory<>("designMat"));
        nbrHeureMatCol.setCellValueFactory(new PropertyValueFactory<>("nbrHeureMat"));

        tableListMat.setItems(matiereList);
    }
}
