package Controller;

import Model.VH;
import javafx.animation.FadeTransition;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.BooleanBinding;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Duration;
import required.DBConnect;

import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class VHController implements Initializable {
    @FXML
    Parent parent;
    @FXML
    Button send;

    private TableView<VH> tableListVH;
    private ObservableList<VH> VHList;
    private TableColumn<VH, String> matProfCol;
    private TableColumn<VH, String> numMatiereCol;
    private TableColumn<VH, Integer> anneeCol;
    private String nomProfInitEdit;
    private String designMatInitEdit;
    private String anneeInitEdit;
    @FXML
    private ChoiceBox nomProf;
    @FXML
    private ChoiceBox designMat;
    @FXML
    private TextField annee;

    public void requestParent(Parent parent) {
        this.parent = parent;
    }

    public void request(TableView<VH> tableListVH,
                        ObservableList<VH> VHList,
                        TableColumn<VH, String> matProfCol,
                        TableColumn<VH, String> numMatiereCol,
                        TableColumn<VH, Integer> anneeCol) {
        this.tableListVH = tableListVH;
        this.VHList = VHList;
        this.matProfCol = matProfCol;
        this.numMatiereCol = numMatiereCol;
        this.anneeCol = anneeCol;

        //System.out.println("request value scope");
    }

    /*--------------Les bouttons dans popup---------------*/
    //Envoie de donnee dans ajout
    public void send(ActionEvent actionEvent) throws SQLException {
        DBConnect dbConnect = new DBConnect();
        String query = "INSERT INTO volumeHoraire (matricule, numMat, annee) VALUES (?,?,?)";
        String matProf = "";
        String numMat = "";
        int annee = 0;

        ChoiceBox choiceBox1 = (ChoiceBox) parent.lookup("#nomProf");
        matProf = (String) choiceBox1.getValue();

        ChoiceBox choiceBox2 = (ChoiceBox) parent.lookup("#designMat");
        numMat = (String) choiceBox2.getValue();

        TextField textField = (TextField) parent.lookup("#annee");
        annee = Integer.parseInt(textField.getText());

        int sucAjout = dbConnect.edit_bdd(query, matProf, numMat, annee);

        if (sucAjout == 1) {
            closePopup(actionEvent);
            alertVHSuccess();
            ajoutVHAfficheList();
        } else {
            alertVHError();
        }
    }

    public void sendUpdate(ActionEvent actionEvent) throws SQLException {
        String matProf = "";
        String numMat = "";
        int annee = 0;

        ChoiceBox choiceBox1 = (ChoiceBox) parent.lookup("#nomProf");
        matProf = (String) choiceBox1.getValue();

        ChoiceBox choiceBox2 = (ChoiceBox) parent.lookup("#designMat");
        numMat = (String) choiceBox2.getValue();

        TextField textField = (TextField) parent.lookup("#annee");
        annee = Integer.parseInt(textField.getText());

        System.out.printf("%s = %s\n%n", matProf, nomProfInitEdit);
        System.out.printf("%s = %s\n%n", numMat, designMatInitEdit);
        System.out.printf("%s = %s\n%n", annee, anneeInitEdit);

        boolean matEq = !matProf.equalsIgnoreCase(nomProfInitEdit);
        boolean numEq = !numMat.equalsIgnoreCase(designMatInitEdit);
        boolean annEq = !String.valueOf(annee).equalsIgnoreCase(anneeInitEdit);

        DBConnect dbConnect = new DBConnect();

        if (matEq)
            if (numEq) {
                if (annEq) {
                    if (dbConnect.edit_bdd("UPDATE `volumeHoraire` SET `matricule`=?, `numMat`=?, `annee`=? WHERE `matricule` = ? AND `numMat`=?",
                            matProf,
                            numMat,
                            annee,
                            nomProfInitEdit,
                            designMatInitEdit) == 0) {
                        alertVHError();
                    } else {
                        ajoutVHAfficheList();
                        closePopup(actionEvent);
                        alertVHSuccess();
                    }
                } else {
                    if (dbConnect.edit_bdd("UPDATE `volumeHoraire` SET `matricule`=?, `numMat`=? WHERE `matricule` = ? AND `numMat`=?",
                            matProf,
                            numMat,
                            String.valueOf(nomProfInitEdit),
                            String.valueOf(designMatInitEdit)) == 0) {
                        alertVHError();
                    } else {
                        ajoutVHAfficheList();
                        closePopup(actionEvent);
                        alertVHSuccess();
                    }
                }
            } else {
                if (annEq) {
                    if (dbConnect.edit_bdd("UPDATE `volumeHoraire` SET `matricule`=?, `annee`=? WHERE `matricule` = ? AND `numMat`=?",
                            matProf,
                            annee,
                            String.valueOf(nomProfInitEdit),
                            designMatInitEdit) == 0) {
                        alertVHError();
                    } else {
                        ajoutVHAfficheList();
                        closePopup(actionEvent);
                        alertVHSuccess();
                    }
                } else {
                    if (dbConnect.edit_bdd("UPDATE `volumeHoraire` SET `matricule`=? WHERE `matricule` = ? AND `numMat`=?",
                            matProf,
                            String.valueOf(nomProfInitEdit),
                            designMatInitEdit) == 0) {
                        alertVHError();
                    } else {
                        ajoutVHAfficheList();
                        closePopup(actionEvent);
                        alertVHSuccess();
                    }
                }
            }
        else {
            if (numEq) {
                if (annEq) {
                    if (dbConnect.edit_bdd("UPDATE `volumeHoraire` SET `numMat`=?, `annee`=? WHERE `matricule` = ? AND `numMat`=?",
                            numMat,
                            annee,
                            nomProfInitEdit,
                            String.valueOf(designMatInitEdit)) == 0) {
                        alertVHError();
                    } else {
                        ajoutVHAfficheList();
                        closePopup(actionEvent);
                        alertVHSuccess();
                    }
                } else {
                    if (dbConnect.edit_bdd("UPDATE `volumeHoraire` SET `numMat`=? WHERE `matricule` = ? AND `numMat`=?",
                            numMat,
                            nomProfInitEdit,
                            String.valueOf(designMatInitEdit)) == 0) {
                        alertVHError();
                    } else {
                        ajoutVHAfficheList();
                        closePopup(actionEvent);
                        alertVHSuccess();
                    }
                }
            } else {
                if (annEq) {
                    if (dbConnect.edit_bdd("UPDATE `volumeHoraire` SET `annee`=? WHERE `matricule` = ? AND `numMat`=?",
                            annee,
                            nomProfInitEdit,
                            designMatInitEdit) == 0) {
                        alertVHError();
                    } else {
                        ajoutVHAfficheList();
                        closePopup(actionEvent);
                        alertVHSuccess();
                    }
                } else {
                    alertVHError();
                }
            }
        }
    }

    public void removeItem(ActionEvent actionEvent) throws SQLException {
        DBConnect dbConnect = new DBConnect();
        String query = "DELETE FROM volumeHoraire WHERE matricule=? AND numMat=?";
        String matProf = "";
        String numMat = "";

        ChoiceBox choiceBox1 = (ChoiceBox) parent.lookup("#nomProf");
        matProf = (String) choiceBox1.getValue();

        ChoiceBox choiceBox2 = (ChoiceBox) parent.lookup("#designMat");
        numMat = (String) choiceBox2.getValue();


        if (dbConnect.removeItemFromBdd(query, matProf, numMat) == 0) {
            alertVHError();
        } else {
            ajoutVHAfficheList();
            alertVHSuccess();
        }
        closePopup(actionEvent);
    }
    /*-----------------------Validate input----------------------------------------*/

    @FXML
    private void closePopup(ActionEvent event) {
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        FadeTransition ft = new FadeTransition(Duration.millis(1000), stage.getScene().getRoot());
        ft.setFromValue(1.0);
        ft.setToValue(0.0);
        ft.setOnFinished(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                stage.close();
            }
        });
        ft.play();
    }

    void alertVHSuccess() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Succès");
        alert.setHeaderText(null);
        alert.initModality(Modality.APPLICATION_MODAL);
        alert.setContentText("L'opération a été effectuée avec succès !");
        alert.showAndWait();
    }

    void alertVHError() {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Erreur");
        alert.setHeaderText(null);
        alert.setContentText("Une erreur est survenu!\nVeuiller réessayer à nouveau s'il vous plaît!");
        alert.showAndWait();
    }

    public boolean validateAnneeVH(String st) {
        if (nomProfInitEdit != null) {
            if (!nomProfInitEdit.equals(nomProf.getValue()) && designMat.getValue().equals(designMatInitEdit) && annee.getText().equals(anneeInitEdit)) {
                return true;
            }
        }
        DBConnect dbConnect = new DBConnect();
        List<String> allNomProf = new ArrayList<>();
        List<String> allNumMat = new ArrayList<>();
        ArrayList<ArrayList<String>> allAnneeNum = new ArrayList<>();
        try {
            ResultSet resultSet1 = dbConnect.fetch_in_bdd("SELECT annee,numMat FROM volumeHoraire;");
            int i = 0;
            while (resultSet1.next()) {
                allAnneeNum.add(new ArrayList<>());
                allAnneeNum.get(i).add(String.valueOf(resultSet1.getInt("annee")));
                allAnneeNum.get(i).add(resultSet1.getString("numMat"));
                i++;
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        boolean b = false;
        for (ArrayList<String> numAnn : allAnneeNum) {
            if ((numAnn.get(0).equalsIgnoreCase(st)) && numAnn.get(1).equals(designMat.getValue())) b = true;
        }
        // Le anneeVH est vide ou nul
        return !st.trim().isEmpty() && st.trim().matches("2[0-9][0-9][0-9]") && !b;
    }

    public boolean validateChoiceVH(String st) {
        if (nomProfInitEdit != null) {
            if (!nomProfInitEdit.equals(nomProf.getValue()) && designMat.getValue().equals(designMatInitEdit) && annee.getText().equals(anneeInitEdit)) {
                return true;
            }
        }
        // Le MatrHVH est vide ou nul
        if (st != null) return !st.isEmpty() && !st.isBlank();
        return false;
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        /*
         * On verifie le fenetre actif a l'aide de style du boutton send
         * Pour eliminer tous les filtres si le fenetre actif est le fenetre delete
         */
        if (!send.getStyle().equals("-fx-background-color: #ff3860;")) {
            //System.out.println("Initialize scope");
            DBConnect dbConnect = new DBConnect();
            List<String> allNomProf = new ArrayList<>();
            List<String> allNumMat = new ArrayList<>();
            ArrayList<ArrayList<String>> allAnneeNum = new ArrayList<>();
            try {
                ResultSet resultSet = dbConnect.fetch_in_bdd("SELECT matricule FROM Professeurs;");
                ResultSet resultSet0 = dbConnect.fetch_in_bdd("SELECT numMat FROM Matieres;");
                ResultSet resultSet1 = dbConnect.fetch_in_bdd("SELECT annee,numMat FROM volumeHoraire;");
                while (resultSet.next()) {
                    allNomProf.add(resultSet.getString("matricule"));
                }
                while (resultSet0.next()) {
                    allNumMat.add(resultSet0.getString("numMat"));
                }
                int i = 0;
                while (resultSet1.next()) {
                    allAnneeNum.add(new ArrayList<>());
                    allAnneeNum.get(i).add(String.valueOf(resultSet1.getInt("annee")));
                    allAnneeNum.get(i).add(resultSet1.getString("numMat"));
                    i++;
                }
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
            ObservableList<String> listNomProf = FXCollections.observableArrayList(allNomProf);
            ObservableList<String> listNumMat = FXCollections.observableArrayList(allNumMat);

            nomProf.setItems(listNomProf);
            designMat.setItems(listNumMat);

            nomProf.valueProperty().addListener((observable, oldValue, newValue) -> {
                if (nomProfInitEdit != null) {
                    if (!(!nomProfInitEdit.equals(newValue) && designMat.getValue().equals(designMatInitEdit) && annee.getText().equals(anneeInitEdit))) {
                        boolean b = false;
                        for (ArrayList<String> numAnn : allAnneeNum) {
                            if ((numAnn.get(0).equals(annee.getText())) && numAnn.get(1).equals(designMat.getValue()))
                                b = true;
                            //System.out.println(designMat.getValue());
                        }
                        if (annee.getText().trim().isEmpty() || !annee.getText().trim().matches("2[0-9][0-9][0-9]") || b) {
                            annee.setStyle("-fx-border-color: red ; -fx-border-width: 2px ;");
                        } else {
                            annee.setStyle("");
                        }
                    }
                } else {
                    boolean b = false;
                    for (ArrayList<String> numAnn : allAnneeNum) {
                        if ((numAnn.get(0).equals(annee.getText())) && numAnn.get(1).equals(designMat.getValue()))
                            b = true;
                        //System.out.println(designMat.getValue());
                    }
                    if (annee.getText().trim().isEmpty() || !annee.getText().trim().matches("2[0-9][0-9][0-9]") || b) {
                        annee.setStyle("-fx-border-color: red ; -fx-border-width: 2px ;");
                    } else {
                        annee.setStyle("");
                    }
                }
            });
            designMat.valueProperty().addListener((observable, oldValue, newValue) -> {
                if (nomProfInitEdit != null) {
                    if (!(!nomProfInitEdit.equals(nomProf.getValue()) && newValue.equals(designMatInitEdit) && annee.getText().equals(anneeInitEdit))) {
                        boolean b = false;
                        for (ArrayList<String> numAnn : allAnneeNum) {
                            if ((numAnn.get(0).equals(annee.getText())) && numAnn.get(1).equals(newValue)) b = true;
                            //System.out.println(designMat.getValue());
                        }
                        if (annee.getText().trim().isEmpty() || !annee.getText().trim().matches("2[0-9][0-9][0-9]") || b) {
                            annee.setStyle("-fx-border-color: red ; -fx-border-width: 2px ;");
                        } else {
                            annee.setStyle("");
                        }
                    }
                } else {
                    boolean b = false;
                    for (ArrayList<String> numAnn : allAnneeNum) {
                        if ((numAnn.get(0).equals(annee.getText())) && numAnn.get(1).equals(newValue)) b = true;
                        //System.out.println(designMat.getValue());
                    }
                    if (annee.getText().trim().isEmpty() || !annee.getText().trim().matches("2[0-9][0-9][0-9]") || b) {
                        annee.setStyle("-fx-border-color: red ; -fx-border-width: 2px ;");
                    } else {
                        annee.setStyle("");
                    }
                }
            });

            annee.textProperty().addListener((observable, oldValue, newValue) -> {
                if (nomProfInitEdit != null) {
                    if (!(!nomProfInitEdit.equals(nomProf.getValue()) && designMat.getValue().equals(designMatInitEdit) && newValue.equals(anneeInitEdit))) {
                        boolean b = false;
                        for (ArrayList<String> numAnn : allAnneeNum) {
                            if ((numAnn.get(0).equals(newValue)) && numAnn.get(1).equals(designMat.getValue()))
                                b = true;
                            //System.out.println(designMat.getValue());
                        }
                        if (newValue.trim().isEmpty() || !newValue.trim().matches("2[0-9][0-9][0-9]") || b) {
                            annee.setStyle("-fx-border-color: red ; -fx-border-width: 2px ;");
                        } else {
                            annee.setStyle("");
                        }
                    }
                } else {
                    boolean b = false;
                    for (ArrayList<String> numAnn : allAnneeNum) {
                        if ((numAnn.get(0).equals(newValue)) && numAnn.get(1).equals(designMat.getValue()))
                            b = true;
                        //System.out.println(designMat.getValue());
                    }
                    if (newValue.trim().isEmpty() || !newValue.trim().matches("2[0-9][0-9][0-9]") || b) {
                        annee.setStyle("-fx-border-color: red ; -fx-border-width: 2px ;");
                    } else {
                        annee.setStyle("");
                    }
                }
            });

            BooleanBinding inputsValid = Bindings.createBooleanBinding(() -> {
                boolean inputM = validateAnneeVH(annee.getText());
                boolean inputN = validateChoiceVH((String) nomProf.getValue());
                boolean inputMa = validateChoiceVH((String) designMat.getValue());

                return inputM && inputMa && inputN;
            }, annee.textProperty(), nomProf.valueProperty(), designMat.valueProperty());
            // Lier la propriété "disable" du bouton à l'expression d'entrées valides
            send.disableProperty().bind(inputsValid.not());
        }

    }


    /*--------------------------RELOAD MAT VIEW TABLE-----------------------------*/

    public ObservableList<VH> ajoutVHList() throws SQLException {
        ObservableList<VH> listData = FXCollections.observableArrayList();

        String sql = "SELECT * FROM volumeHoraire ORDER BY dateAjout";

        DBConnect dbConnect = new DBConnect();
        ResultSet resultSet = dbConnect.fetch_in_bdd(sql);

        VH VHData;
        while (resultSet.next()) {
            VHData = new VH(
                    resultSet.getInt("annee"),
                    resultSet.getString("matricule"),
                    resultSet.getString("numMat"));

            listData.add(VHData);
        }
        return listData;
    }


    public void ajoutVHAfficheList() throws SQLException {
        VHList = ajoutVHList();

        anneeCol.setCellValueFactory(new PropertyValueFactory<>("anneeVH"));
        matProfCol.setCellValueFactory(new PropertyValueFactory<>("matProf"));
        numMatiereCol.setCellValueFactory(new PropertyValueFactory<>("numMatiere"));

        tableListVH.setItems(VHList);
    }

    public void setEditVal(String nomProfInitEdit, String designMatInitEdit, String anneeInitEdit) {
        this.nomProfInitEdit = nomProfInitEdit;
        this.designMatInitEdit = designMatInitEdit;
        this.anneeInitEdit = anneeInitEdit;
    }
}
